class HelloWorld
  def initialize
    output_lines
    # Super important comment 5
  end

  def output_lines
    puts 'Conflict1'
    puts 'Conflict2'
    puts 'Conflict3'
    puts 'Conflict4'
    puts 'Conflict5'
  end
end
