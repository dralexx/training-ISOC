class HelloWorld
  def initialize(name)
    @name = name.capitalize
  end
  # working here
  def say_hi
    puts "Second try: Hello #{@name}!"
  end

  # TODO: Remove this comment and the method below.
  def say_bye
    puts "Second Try: Good-bye #{@name}!"
  end
end
